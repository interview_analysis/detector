import pickle
from src.processing import lemmatize

with open("keywords.pickle", "rb") as f:
    keywords_list = pickle.load(f)

def detect(tokens, topk=10):
    doc_keyword_indexes = []
    for doc_tokens in tokens:
        lemmaized_tokens = lemmatize(doc_tokens)
        relevant_keywords = [ _[0] for _ in keywords_list[:topk] ]
        result = [ index for index, keyword in enumerate(lemmaized_tokens) if keyword in relevant_keywords  ]
        doc_keyword_indexes.append(result)
    return doc_keyword_indexes
