import numpy as np
import pickle
from src.embedding import muse
from src.phrase import PhraseExtractor
from model import ModelCompose


labels = [
    'readyness_for_changes',
    'self_organization',
    'motivation',
]

with open("model.pickle", "rb") as f:
    model = pickle.load(f)
extractor = PhraseExtractor(fraction=1)


def detect(tokens):
    phrases_list, phrases_indexes = extractor.transform(tokens, multiple_docs=True, return_indexes=True)
    phrases_embeddings = muse(phrases_list, batched=True)
    indexes, scores = model.find_most_probable(phrases_embeddings, topk=1)
    indexes, scores = indexes[0][:-1], scores[0][:-1]
    results = []
    for label_index, (index, score) in enumerate(zip(indexes, scores)):
        start_index, end_index, doc_index = phrases_indexes[index]
        results.append({
            'label': labels[label_index],
            'doc_index': doc_index,
            'start_index': start_index,
            'end_index': end_index,
            'probability': score,
        })
    return results

# Warmup
detect([[ "здравствуйте", "уважаемые", "коллеги", "я", "акимов", "татьяна", "ивановна", "профессор", "кафедры", "русской", "и", "зарубежной", "литературы", "филологического", "факультета", "мордовского", "государственного", "университета", "стаж", "научно-педагогической", "деятельности", "за", "семнадцать", "лет", "из", "которых", "десять", "лет", "я", "веду", "дисциплину", "информационно-коммуникационные", "технологии", "биологических", "исследованиях", "для", "студентов", "бакалавров", "на", "втором", "курсе", "родился", "с", "дисциплиной", "экологически", "прыщ", "монарх", "который", "был", "предназначен", "для", "подготовки", "студентов", "к", "ведению", "физиологической", "деятельности", "кроме", "дисциплина", "данной", "была", "написана", "методичка", "для", "уже", "магистрантов", "информационные", "ресурсы", "социологическом", "исследовании", "и", "буквально", "год", "назад", "разрабатываются", "дисциплина", "для", "аспирантов", "информационные", "технологии", "в", "науке", "и", "образованию", "поэтому", "большим", "интересом", "пройду", "обучение", "о", "предложенном", "по", "данному", "курсу" ]])
