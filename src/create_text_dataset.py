import pandas as pd
from pathlib import Path

text_folder = Path("data/text_raw")
user_df = pd.read_pickle("datasets/user_dataset.pickle")

emails = []
task_texts = [[], [], []]
for user_folder in text_folder.iterdir():
    emails.append(user_folder.name)
    for i in range(3):
        task_file = user_folder / f"task{i+1}.txt"
        if task_file.exists():
            with task_file.open() as f:
                task_texts[i].append(f.read())
        else:
            task_texts[i].append(None)

text_df = pd.DataFrame({
    'email': emails,
    'task1_text': task_texts[0],
    'task2_text': task_texts[1],
    'task3_text': task_texts[2],
})

text_df.to_pickle("datasets/raw_text_dataset.pickle")