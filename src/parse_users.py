import time
import pandas
from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from defines import url, url_base, init_driver

options = Options()
options.headless = True


ids, emails, firstnames, secondnames, thirdnames = [], [], [], [], []
with webdriver.Firefox(options=options) as driver:
    init_driver(driver)
    driver.get(url)
    time.sleep(10)
    
    student_list_elem = driver.find_element_by_tag_name("tbody")
    video_urls = []
    for i, student_elem in enumerate(student_list_elem.find_elements_by_tag_name("tr")):
        # if i == 4: break
        info_elem = student_elem.find_elements_by_tag_name("td")

        secondnames.append(info_elem[0].text)
        firstnames.append(info_elem[1].text)
        thirdnames.append(info_elem[2].text)
        emails.append(info_elem[3].text)

        student_id = info_elem[4].find_elements_by_tag_name('a')[2].get_attribute('href').split("=")[-1]
        ids.append(student_id)

df = pandas.DataFrame({
    "email": emails,
    "firstname": firstnames,
    "secondname": secondnames,
    "thirdname": thirdnames,
    "id": ids,
})


df.to_pickle("user_dataset.pickle")
