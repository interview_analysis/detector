def tokenize(string):
    if string is None:
        return None
    return string.strip().lower().replace(",", "").split(" ")


def stringify(tokens):
    if tokens is None:
        return None
    return " ".join(tokens)


__lemmatize_nlp = None
def lemmatize(tokens):
    global __lemmatize_nlp
    if __lemmatize_nlp is None:
        import spacy
        __lemmatize_nlp = spacy.load('ru_core_news_md')

    lemmas = []
    step = 10000
    for i in range(0, 10000, step):
        text = " ".join(tokens[i : i+step])
        doc = __lemmatize_nlp(text)
        lemmas.extend([ token.lemma_ for token in doc ])

    return lemmas


def flatten(texts):
    if type(texts) == str:
        return texts
    elif hasattr(texts, '__iter__'):
        return " ".join([flatten(text) for text in texts if text is not None])
    else:
        raise ValueError("Argument must be string or iterable of string")
