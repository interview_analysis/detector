import matplotlib.pyplot as plt
from matplotlib import colors
from sklearn.manifold import TSNE

def plot_distribution(df, reducer=TSNE(metric='cosine'), alpha=1, size=10):
    full_data = []
    for column in df.columns:
        series = df[column]
        full_data.extend( series[series.notna()].tolist() )
    transformed_data = reducer.fit_transform(full_data)

    current_data_index = 0
    color_names = list(colors.TABLEAU_COLORS.keys())
    color_values = list(colors.TABLEAU_COLORS.values())    
    for i, column in enumerate(df.columns):
        color_index = i % len(color_names)
        print(f"{df.columns[i]} - {color_names[color_index]}")
        data = transformed_data[current_data_index:current_data_index+df[column].count()]
        current_data_index += df[column].count()
        plt.scatter(data[:,0], data[:, 1], edgecolors=color_values[color_index], alpha = alpha, s=size)
