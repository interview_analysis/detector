import numpy as np
from navec import Navec
from joblib import Memory
from .constants import cache
from .processing import tokenize

memory = Memory(location=cache.as_posix(), verbose=0)


word2vec_model = None

@memory.cache
def word2vec(phrase, batched=False):
    if phrase is None:
        return None
    
    global word2vec_model
    if word2vec_model is None:
        word2vec_model = Navec.load("navec_hudlit_v1_12B_500K_300d_100q.tar")

    def transform(phrase):
        relevant_words = [ word2vec_model[token] for token in tokenize(phrase) if token in word2vec_model.vocab ]
        if len(relevant_words) == 0:
            return None

        embedding = np.array(relevant_words).mean(0)
        return embedding

    if batched:
        return [ transform(p) for p in phrase ]
    else:
        return transform(phrase)



muse_model_link = "https://tfhub.dev/google/universal-sentence-encoder-multilingual-large/3"
muse_model = None

@memory.cache
def muse(phrase, batched=False):
    if phrase is None:
        return None

    global muse_model
    if muse_model is None:
        import tensorflow_hub as hub
        import tensorflow_text
        muse_model = hub.load(muse_model_link)

    if batched:
        return muse_model([phrase]).numpy().tolist()
    else:
        return muse_model([phrase]).numpy().squeeze()


rubert_model_name = "DeepPavlov/rubert-base-cased-sentence"
rubert_tokenizer = None
rubert_model = None

@memory.cache
def rubert(words_list):
    global rubert_tokenzer, rubert_model
    
    if rubert_model is None:
        import transformers
        rubert_tokenizer = transformers.AutoTokenizer.from_pretrained(rubert_model_name)
        rubert_model = transformers.AutoModel.from_pretrained(rubert_model_name, return_dict=True)

    result = []
    for words in words_list:
        if type(words) != str:
            result.append(None)
            continue
        encoded = rubert_tokenizer.encode_plus(words, return_tensors='pt')
        embedding = rubert_model(**encoded)[1][0].detach().numpy()
        result.append(embedding)

    return result
