from urllib.parse import urlparse


url = "https://learn.innopolis.university/Instructors/Trainings/3308018f-ae34-43f5-e8a2-08d8224caade/Students?back=%2FInstructors%2FTrainings%2F3308018f-ae34-43f5-e8a2-08d8224caade%3Fback%3D%252FInstructors%252FSets%252F6ab37ff8-3f74-482f-8e02-08d8224c8eb7%253Fback%253D%25252FInstructors"
parsed_url = urlparse(url)
url_base = parsed_url.scheme + "://" + parsed_url.netloc

cookies = [
    { "name": ".AspNet.Consent", "value": "yes", "domain": "learn.innopolis.university", "path": "/"},
    { "name": ".AspNetCore.Identity.Application", "value":
"CfDJ8GWvii3rPUpMrWkWCGMbPXECWfLpz_ofbhk0ZX-gG7X6NgtiNRJMg-EGfncWZc0EMN7pJ1XfSzAop3tCzNg2Kg5rw9Tg3kx2OsBonWJnjPrcGCmtWzV3pTes1OJ-Bd3WAgiDJDPpvPbKLJBk3ZA7km60doc7Aa1kFyANaNduxZEIyeffdoqzjWO86PGV112vwmi46QqKD9aIFUml7_knmnMc5cnpmzsGfSAORXUs8tKtongl751-LVbIsM3IhX0HVIjkar6nZioewwdMKKWOQDEanP-3tGr5Uu27yh0tZI-7mAkDaBKAT0ylTLiJR1z3XbO27J25LF2Om1sOnZEW0n5cxubblThARs-cBqPW2vpW5ZFXw2S0H8zhtaQaRoVFuwGwErQ8uCCOIPwmVsShwJv_aAJtBkGZjMPePFwXxP1uv7Lmn6Jh9vFnuxYUoXh0A3i_IwPPW2u7WcCPU-xemUZf7-AIadnmp945usAUCY4wc-NV241djvi4guJqupawbisguCNIoCd0ND5mcIx7vqaeIY17--ldDjRXSLoKeOv4bd1ndKxS10rFg9cH5NSonLz_l7tzV-HxGng-JBwRzT4OCZONlr0-6Bp-Kgy8kGhR6LVqJzSWSPcNQ__fEHKsQzaCG1v4r8Vq3_xW4OUOtzfReC4gmHJyKw6d8XtfqJuL4_VuiaO1nnufkZMcCjIe-HV3x__Zcz_tzlkbdwTmLVDkMKnsO6qyMOSe9a1MFUIP1NrlV9Ji0tmxIWT1QWN5h9z0Q6OA_Aop3MxFQsHgo9418oAACBP33GFeGBaKnIkF3RiUZUyRaidqtwWVKF46ed_wijRitx3DxYIvZuJNNXRHESOCr5DKUOCFJIw-2BXyhs0l7nZN9kSrdfbRHMNRSlFki2e7RhwlZBW7S_uhV56whdBQ9Dduur09i_T0pZSbrJmQ07yYXmEiYUjkfs83E-eIq73GbbpRCUuIxIvaeIL6w_ZxxV7_gVN48Nh6o78KBNnA7WE2jZ72LLAhD0fqxs8hRqCON7wo4DBLqQiUFTY", "domain": "learn.innopolis.university", "path": "/" },
    { "name": ".AspNetCore.Antiforgery.m88UW4eLVro", "value": "CfDJ8GWvii3rPUpMrWkWCGMbPXGGLzMm-IBp_Zb9XAxO9UF16JwOqkhlJoLurW-a0_iefLvrPu0VEV4WAcghXTsuRjB4jVNj7tZ8QC3HdEzP0dsh6XjPM4dwWNWbzGLpkRpPUnl8yqO5RJO1bjcgy6CEb8s", "domain": "learn.innopolis.university", "path": "/" },
]

video_path = "data/videos/"


def init_driver(driver):
    driver.get(url_base)
    for cookie in cookies:
        driver.add_cookie(cookie)
