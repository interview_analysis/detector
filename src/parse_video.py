import time
import pandas
from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from bs4 import BeautifulSoup
from urllib.parse import urlparse
from urllib.request import urlretrieve
from concurrent.futures import ThreadPoolExecutor, ProcessPoolExecutor
from pathlib import Path


url = "https://learn.innopolis.university/Instructors/Trainings/3308018f-ae34-43f5-e8a2-08d8224caade/Students?back=%2FInstructors%2FTrainings%2F3308018f-ae34-43f5-e8a2-08d8224caade%3Fback%3D%252FInstructors%252FSets%252F6ab37ff8-3f74-482f-8e02-08d8224c8eb7%253Fback%253D%25252FInstructors"
parsed_url = urlparse(url)
url_base = parsed_url.scheme + "://" + parsed_url.netloc

cookies = [
    { "name": ".AspNet.Consent", "value": "yes", "domain": "learn.innopolis.university", "path": "/"},
    { "name": ".AspNetCore.Identity.Application", "value":
"CfDJ8GWvii3rPUpMrWkWCGMbPXECWfLpz_ofbhk0ZX-gG7X6NgtiNRJMg-EGfncWZc0EMN7pJ1XfSzAop3tCzNg2Kg5rw9Tg3kx2OsBonWJnjPrcGCmtWzV3pTes1OJ-Bd3WAgiDJDPpvPbKLJBk3ZA7km60doc7Aa1kFyANaNduxZEIyeffdoqzjWO86PGV112vwmi46QqKD9aIFUml7_knmnMc5cnpmzsGfSAORXUs8tKtongl751-LVbIsM3IhX0HVIjkar6nZioewwdMKKWOQDEanP-3tGr5Uu27yh0tZI-7mAkDaBKAT0ylTLiJR1z3XbO27J25LF2Om1sOnZEW0n5cxubblThARs-cBqPW2vpW5ZFXw2S0H8zhtaQaRoVFuwGwErQ8uCCOIPwmVsShwJv_aAJtBkGZjMPePFwXxP1uv7Lmn6Jh9vFnuxYUoXh0A3i_IwPPW2u7WcCPU-xemUZf7-AIadnmp945usAUCY4wc-NV241djvi4guJqupawbisguCNIoCd0ND5mcIx7vqaeIY17--ldDjRXSLoKeOv4bd1ndKxS10rFg9cH5NSonLz_l7tzV-HxGng-JBwRzT4OCZONlr0-6Bp-Kgy8kGhR6LVqJzSWSPcNQ__fEHKsQzaCG1v4r8Vq3_xW4OUOtzfReC4gmHJyKw6d8XtfqJuL4_VuiaO1nnufkZMcCjIe-HV3x__Zcz_tzlkbdwTmLVDkMKnsO6qyMOSe9a1MFUIP1NrlV9Ji0tmxIWT1QWN5h9z0Q6OA_Aop3MxFQsHgo9418oAACBP33GFeGBaKnIkF3RiUZUyRaidqtwWVKF46ed_wijRitx3DxYIvZuJNNXRHESOCr5DKUOCFJIw-2BXyhs0l7nZN9kSrdfbRHMNRSlFki2e7RhwlZBW7S_uhV56whdBQ9Dduur09i_T0pZSbrJmQ07yYXmEiYUjkfs83E-eIq73GbbpRCUuIxIvaeIL6w_ZxxV7_gVN48Nh6o78KBNnA7WE2jZ72LLAhD0fqxs8hRqCON7wo4DBLqQiUFTY", "domain": "learn.innopolis.university", "path": "/" },
    { "name": ".AspNetCore.Antiforgery.m88UW4eLVro", "value": "CfDJ8GWvii3rPUpMrWkWCGMbPXGGLzMm-IBp_Zb9XAxO9UF16JwOqkhlJoLurW-a0_iefLvrPu0VEV4WAcghXTsuRjB4jVNj7tZ8QC3HdEzP0dsh6XjPM4dwWNWbzGLpkRpPUnl8yqO5RJO1bjcgy6CEb8s", "domain": "learn.innopolis.university", "path": "/" },
]

options = Options()
# options.headless = True

video_path = "data/videos/"

def init_driver(driver):
    driver.get(url_base)
    for cookie in cookies:
        driver.add_cookie(cookie)

def repeat(attempts=10):
    def inner(func):
        def f(*args, **kwargs):
            counter = 0
            while True:
                try:
                    res = func(*args, **kwargs)
                    return res
                except Exception as e:
                    counter += 1
                    if counter > attempts:
                        raise e
                time.sleep(5)
        return f
    return inner


# @repeat(attempts=2)
def parse_user_videos(url, timeout=30):
    with webdriver.Firefox(options=options) as driver:
        init_driver(driver)

        def parse_user_video(url):
            print(url)
            driver.get(url)
            WebDriverWait(driver, timeout).until(
                EC.presence_of_element_located([By.XPATH, "//a[contains(@onclick, 'preview')]"]))
            preview_element = driver.find_element_by_xpath("//a[contains(@onclick, 'preview')]")
            preview_element.click()
            driver.switch_to.frame(driver.find_element_by_tag_name("iframe"))
            WebDriverWait(driver, timeout).until(EC.presence_of_element_located([By.TAG_NAME, "a"]))
            time.sleep(1)
            file_link = driver.find_element_by_tag_name('a').get_attribute("href")
            if file_link.endswith("#"):
                return None
            return file_link
            
        print(url)
        driver.get(url)
        WebDriverWait(driver, timeout).until(EC.presence_of_element_located([By.XPATH, "//td[@class=' table-tools']"]))
        time.sleep(2)
        rows = driver.find_elements_by_tag_name('tr')
        video_urls = []
        for row in rows[1:4]:
            links = row.find_elements_by_tag_name('a')
            if len(links) != 0:
                video_urls.append(links[0].get_attribute('href'))
            else:
                video_urls.append(None)
        
        links = []
        for url in video_urls:
            if url is not None:
                links.append(parse_user_video(url))
            else:
                links.append(None)
            time.sleep(1)
    return links


def parse_users(url, cookies):

    emails, firstnames, secondnames, thirdnames = [], [], [], []

    with webdriver.Firefox(options=options) as driver:
        init_driver(driver)
        driver.get(url)
        time.sleep(6)
        
        student_list_elem = driver.find_element_by_tag_name("tbody")
        video_urls = []
        for i, student_elem in enumerate(student_list_elem.find_elements_by_tag_name("tr")):
            if i == 4: break
            info_elem = student_elem.find_elements_by_tag_name("td")
            secondnames.append(info_elem[0].text)
            firstnames.append(info_elem[1].text)
            thirdnames.append(info_elem[2].text)
            emails.append(info_elem[3].text)
            
            video_url = info_elem[4].find_elements_by_tag_name("a")[2].get_attribute('href')
            video_urls.append(video_url)

        returns = []
        # with ProcessPoolExecutor(max_workers=4) as executor:
            # for video_url in video_urls:
            #     print(video_url)
            #     ret = executor.submit(parse_user_videos, video_url)
            #     returns.append(ret)
        for video_url in video_urls:
            returns.append(parse_user_videos(video_url))
        
        tasks_links = [[None for _ in emails], [None for _ in emails], [None for _ in emails]]
        for i, (email, ret) in enumerate(zip(emails, returns)):
            try:
                links = ret
            except Exception as e:
                links = [None, None, None]
                print(f"Parsing user {email} end up with following error:")
                print(e)
            finally:
                try:
                    tasks_links[0][i] = links[0]
                    tasks_links[1][i] = links[1]
                    tasks_links[2][i] = links[2]
                except Exception as e:
                    tasks_links[0][i] = None
                    tasks_links[1][i] = None
                    tasks_links[2][i] = None
                    print(f"Links {links} for {email} has following error:")
                    print(e)

    df = pandas.DataFrame({
        "email": emails,
        "firstname": firstnames,
        "secondname": secondnames,
        "thirdname": thirdnames,
        "task1_link": tasks_links[0],
        "task2_link": tasks_links[1],
        "task3_link": tasks_links[2]
    })
    return df


df = parse_users(url, cookies)
df.to_pickle("dataset.pickle")
