import pandas as pd


def parse_assessment(filename):
    df = pd.read_excel(filename, 1)
    
    df.drop(columns=df.columns[0], inplace=True)
    
    df.rename(columns={column: df.loc[0, column] for column in df.columns[:5]}, inplace=True)
    df.rename(columns={column: df.loc[1, column] for column in df.columns[5:]}, inplace=True)
    df.rename(columns={'E-mail': 'email'}, inplace=True)
    
    df = df.loc[:, df.columns.notnull()]
    
    df.drop([0,1], inplace=True)
    df.drop(1566, inplace=True)
    
    df.reset_index(inplace=True, drop=True)
    
    df.loc[0::2, df.columns[5:] + " keywords"] = df.loc[1::2, df.columns[5:]].to_numpy()
    
    df = df.loc[0::2]
    df.reset_index(inplace=True, drop=True)
    
    df.iloc[1::2, :3] = df.iloc[0::2, :3].to_numpy()
    
    return df