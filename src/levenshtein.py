import numpy as np


def levenshtein_distance(source, target):

    m, n = len(source)+1, len(target)+1

    d = np.zeros(shape=(m, n), dtype=np.int32)
    d[0, :] = np.arange(n)
    starts = np.zeros_like(d)
    starts[:, 0] = np.arange(m)
 
    for j in range(1, n):
        for i in range(1, m):
            if source[i-1] == target[j-1]:
                substitutionCost = 0
            else:
                substitutionCost = 1

            positions = [ (i-1, j), (i, j-1), (i-1, j-1) ]
            costs = [1, 1, substitutionCost]
            pos_number = np.argmin(list(map(lambda x: d[x[0]]+x[1], zip(positions, costs))))
            d[i, j] = d[positions[pos_number]] + costs[pos_number]
            starts[i, j] = starts[positions[pos_number]]
 
    end = np.argmin(d[:,-1])
    start = starts[end, -1]
    return start, end, d[end, -1]


def find_subsequence(seq_tokens, subseq_tokens):

    subseq_text = " ".join(subseq_tokens)
    seq_text = " ".join(seq_tokens)

    soundex = RussianSoundex()
    subseq_soundex = soundex.transform(subseq_text)
    seq_soundex = soundex.transform(seq_text)
    
    char2word = [None for _ in seq_soundex]
    current_word = 0
    for i in range(len(seq_soundex)):
        char2word[i] = current_word
        if seq_soundex[i] == " ":
            current_word += 1

    left_char, right_char, distance = levenshtein_distance(seq_soundex, subseq_soundex)
    
    return char2word[left_char], char2word[right_char], distance