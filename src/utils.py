import os
import types
from contextlib import contextmanager
import numpy as np
from .transformer import ToAudioTransformer, AudioToTextTransformer, TextToTokensTransformer
from sklearn.pipeline import make_pipeline


def get_full_pipeline():
    return make_pipeline(ToAudioTransformer(), AudioToTextTransformer(model_path="model-ru"), TextToTokensTransformer())


@contextmanager
def transwrite(file, *args, binary=False, **kwargs):
    filename = f"/tmp/temp_writing{os.getpid()}"
    mode = "wb" if binary else "w"
    fd = open(filename, *args, mode=mode, **kwargs)
    success = True
    exception = None
    try:
        yield fd
    except Exception as ex:
        success = False
        exception = ex
    finally:
        if success:
            os.replace(filename, file)
        else:
            raise exception
            
            
class ExpLoss():
    def __init__(self, exp=0.99):
        self.exp = exp
        self.norm_koef = 1
        self.loss = 0
        self.loss_n = 0
        
    def __call__(self, loss):
        self.loss_n += 1
        self.loss = self.loss * self.exp + loss * (1-self.exp)
        self.norm_koef *= self.exp
        return self.loss / (1 - self.norm_koef)
