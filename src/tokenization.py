def tokenize(string):
    return string.strip().lower().replace(",", "").split(" ")
