import time
import pandas as pd
from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from defines import url, url_base, init_driver

options = Options()
options.headless = True

df_users = pd.read_pickle("user_dataset.pickle")

training = "3308018f-ae34-43f5-e8a2-08d8224caade"
lessons   = ["bf6aca55-3f2b-4331-24f3-08d8224b397e", "326a9a3a-98f1-4bff-24f4-08d8224b397e", "6f7d9f18-cf84-4e57-24f5-08d8224b397e"]
works     = ["1d564d4a-9f94-4602-61b5-08d8224b3983", "dffc832a-9041-4832-61b6-08d8224b3983", "b4670dcd-aec5-4e26-61b7-08d8224b3983"]
exercises = ["a137cdb8-4fb6-47d5-afab-08d8224b398a", "0f41dc02-01a6-4ca8-afac-08d8224b398a", "ef97fc2e-249d-49f3-afad-08d8224b398a"]
students = list(df_users.id)
emails = list(df_users.email)
link_template = "https://learn.innopolis.university/Instructors/Trainings/%s/Lessons/%s/Work/%s/Exercises/%s/Results?student=%s"

timeout = 10
tasks_links = [[], [], []]
with webdriver.Firefox(options=options) as driver:
    init_driver(driver)

    for email, student in zip(emails, students):
        print(email)
        for i, [lesson, work, exercise] in enumerate(zip(lessons, works, exercises)):
            print(i)
            link = link_template % (training, lesson, work, exercise, student)
            driver.get(link)

            preview_xpath = "//a[contains(@onclick,'preview')]"
            WebDriverWait(driver, timeout).until(EC.presence_of_element_located([By.XPATH, preview_xpath]))
            preview_element = driver.find_element_by_xpath(preview_xpath)
            time.sleep(1)
            preview_element.click()
            driver.switch_to.frame(driver.find_element_by_tag_name("iframe"))

            download_xpath = "//a[contains(@id,'file-link')]"
            try:
                WebDriverWait(driver, timeout).until(EC.presence_of_element_located([By.XPATH, download_xpath]))
                time.sleep(1)
                url = driver.find_element_by_xpath(download_xpath).get_attribute("href")
                if not url.endswith("#"):
                    tasks_links[i].append(url)
                else:
                    tasks_links[i].append(None)
            except Exception as e:
                tasks_links[i].append(None)

    
df = pd.DataFrame({'id': students,
    'task1_link': tasks_links[0],
    'task2_link': tasks_links[1],
    'task3_link': tasks_links[2],
    })
df.to_pickle("link_dataset.pickle")
