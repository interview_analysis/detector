from itertools import chain
import numpy as np
from numpy.random import choice
from math import ceil
from .processing import stringify


def get_all_phrases(tokens, windows=range(1, 30, 1), window_stride_step=0.25, fraction=1.0):
    def extract_phrases(tokens, window_size, step):
        phrases = []
        for start_index in range(0, len(tokens)-window_size, step):
            end_index = start_index + window_size
            phrases.append( tokens[start_index:end_index] )
        return phrases
    
    if tokens is None:
        return None
    
    if len(tokens) == 0:
        return []
    
    if type(tokens[0]) == str:
        
        if windows.stop > len(tokens):
            windows = range(windows.start, len(tokens), windows.step)
        windows_phrases = []
        for window_size in windows:
            if type(window_stride_step) == int:
                step = min(window_stride_step, window_size)
            elif type(window_stride_step) == float:
                step = int( window_size * window_stride_step )
                if step == 0:
                    step = 1
            else:
                raise TypeError("'window_stride_step' must be either int or float")
            windows_phrases.append( extract_phrases(tokens, window_size, step) )
        result = list(chain( *windows_phrases ))
        if fraction < 1:
            result = choice(result, size=int(fraction * len(result)), replace=False)
        return result
            
    elif hasattr(tokens[0], '__iter__'):
        return list(chain(*[
            get_all_phrases(subtokens, windows=windows, window_stride_step=window_stride_step, fraction=fraction)
        for subtokens in tokens if subtokens is not None ]))
    else:
        raise TypeError("'text' argument must be array of string tokens or higher order array")


class PhraseExtractor():
    def __init__(self, windows=range(1, 30, 1), window_stride_step=0.25, fraction=1.0):
        self.windows = windows
        self.window_stride_step = window_stride_step
        self.fraction = fraction

    def transform(self, tokens, multiple_docs=False, return_indexes=False):
        if tokens is None:
            return None
        if multiple_docs:
            phrases_list = [self.transform(doc_tokens, return_indexes=return_indexes) for doc_tokens in tokens]
            if return_indexes:
                result_phrases, result_indexes = [], []
                for doc_index, val in enumerate(phrases_list):
                    phrases, indexes = val[0], val[1]
                    result_phrases.extend(phrases)
                    result_indexes.extend([
                        (start_index, end_index, doc_index) for (start_index, end_index) in indexes
                    ])
                return result_phrases, result_indexes
            else:
                return list(chain(*phrases_list))
        else:
            windows, window_stride_step, fraction = self.windows, self.window_stride_step, self.fraction

            if windows.stop > len(tokens):
                windows = range(windows.start, len(tokens), windows.step)
            
            phrases = []
            indexes = []
            for window_size in windows:
                if type(window_stride_step) == int:
                    step = min(window_stride_step, window_size)
                elif type(window_stride_step) == float:
                    step = ceil( window_size * window_stride_step )

                for start_index in range(0, len(tokens)-window_size, step):
                    end_index = start_index + window_size
                    phrases.append(stringify( tokens[start_index:end_index] ))
                    indexes.append(( start_index, end_index ))

            if len(phrases) == 0:
                return None

            if fraction < 1:
                idx = np.random.choice(np.arange(len(phrases)), size=ceil(fraction * len(phrases)), replace=False)
                phrases = [ phrases[id_] for id_ in idx ]
                indexes = [ indexes[id_] for id_ in idx ]

            if return_indexes:
                return phrases, indexes
            else:
                return phrases
