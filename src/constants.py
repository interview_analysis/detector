from pathlib import Path

root = Path(__file__).parent.parent

cache = root / '.cache'
if not cache.exists():
    cache.mkdir()
