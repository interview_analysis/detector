from sklearn.base import BaseEstimator, TransformerMixin
from moviepy.editor import *
from moviepy.audio.AudioClip import AudioArrayClip
from pathlib import Path
import json
from vosk import Model, KaldiRecognizer
from natasha import Doc, Segmenter, NewsEmbedding, NewsMorphTagger, MorphVocab


class ToAudioTransformer(BaseEstimator, TransformerMixin):
    
    default_filename = Path("res")
    
    def __init__(self, sample_rate=16000, output_filename='.wav', output_folder="/tmp/interview_analyzer", normalize=True):
        self.sample_rate=sample_rate
        self.output_folder = Path(output_folder)
        self.normalize = normalize
        if output_filename.startswith("."):
            self.output_filename = self.default_filename.with_suffix(output_filename)
        else:
            self.output_filename = Path(output_filename)
    
    def fit(self, X, y = None):
        return self
    
    def transform(self, filename):
        self.output_folder.mkdir(parents=True, exist_ok=True)
        audioclip = AudioFileClip(filename)
        if self.normalize:
            audioclip = audioclip.audio_normalize()
        arr = audioclip.to_soundarray()
        mean_arr = arr.mean(axis=1, keepdims=True)
        res = AudioArrayClip(mean_arr, fps=audioclip.fps*2)
        output_fullname = self.output_folder / self.output_filename
        res.write_audiofile(output_fullname, fps=self.sample_rate, logger=None)
        return str(output_fullname)
    

class AudioToTextTransformer(BaseEstimator, TransformerMixin):
    
    def __init__(self, model_path):
        self.model = Model(model_path)
        
    def fit(self, X, y = None):
        return self
    
    def transform(self, filename):
        with open(filename, 'rb') as wav_file:
            header = wav_file.read(44)
            sample_rate = int.from_bytes(header[24:28], 'little', signed=False)
            rec = KaldiRecognizer(self.model, sample_rate)
            
            while True:
                data = wav_file.read(4000)
                if len(data) == 0:
                    break
                rec.AcceptWaveform(data)
        
        result = json.loads(rec.FinalResult())
        return result['text']
    

class TextToTokensTransformer(BaseEstimator, TransformerMixin):
    
    def __init__(self):
        self.segmenter = Segmenter()
        self.tagger = NewsMorphTagger(NewsEmbedding())
        self.vocab = MorphVocab()
    
    def fit(self, X, y = None):
        return self
    
    def transform(self, text):
        doc = Doc(text)
        doc.segment(self.segmenter)
        doc.tag_morph(self.tagger)
        for token in doc.tokens:
            token.lemmatize(self.vocab)
        result = [token.lemma for token in doc.tokens]
        return result
