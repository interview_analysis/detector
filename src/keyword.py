import math
from collections import Counter, OrderedDict
from .processing import lemmatize

def get_keywords(tokens):
    rows = open('lemma.num', encoding='Windows-1251').readlines()
    rus_words_prob = {}
    for row in rows:
        data = row.split(' ')
        rus_words_prob[data[2]] = float(data[1]) / 1_000_000

    doc_words_count = Counter()
    for lemma in lemmatize(tokens):
        doc_words_count[lemma] += 1

    doc_total_count = sum(doc_words_count.values())
    doc_words_prob = OrderedDict(sorted(
        [(word, count/doc_total_count) for word, count in doc_words_count.items()],
        key=lambda x: x[1], reverse=True)
    )

    doc_words_divergence = OrderedDict(sorted(
        [( word, prob * math.log( prob / rus_words_prob[word] ) if word in rus_words_prob else 0 )
        for word, prob in doc_words_prob.items()],
        key=lambda x: x[1], reverse=True)
    )

    return doc_words_divergence
