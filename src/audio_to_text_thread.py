import sys
from pathlib import Path
import shutil
from transformer import ToAudioTransformer, AudioToTextTransformer
from sklearn.pipeline import make_pipeline
from multiprocessing import Process, Queue, current_process

text_folder = Path("data/text_raw")
audio_folder = Path("data/audio")

queue = Queue()
for image in audio_folder.iterdir():
    queue.put(image.name)
total_size = queue.qsize()

def transformer(queue):

    temp_text_file = f"/tmp/user_text{current_process()}.txt"
    transformer = make_pipeline(ToAudioTransformer(), AudioToTextTransformer("model-ru"))
    while True:
        try:
            email = queue.get_nowait()
        except:
            return

        print(f"{queue.qsize()}/{total_size}: {email}")

        audio_path = audio_folder / email
        text_path = text_folder / email
        text_path.mkdir(exist_ok=True)

        for audio_file in audio_path.iterdir():
            print(audio_file)

            basename = audio_file.stem
            text_file = text_path / (basename + ".txt")

            if text_file.exists():
                continue

            text = transformer.transform(str(audio_file))
            with open(temp_text_file, "w") as f:
                f.write(text)

            shutil.move(temp_text_file, text_file)

thread_number = int(sys.argv[1]) if len(sys.argv) > 1 else 1
processes = []

for _ in range(thread_number):
    process = Process(target=transformer, args=[queue])
    process.start()
    processes.append(process)

for process in processes:
    process.join()
