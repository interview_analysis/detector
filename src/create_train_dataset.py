import os
os.chdir("../datasets")

import pandas as pd

text_df = pd.read_pickle("text_dataset.pickle")
score_df = pd.read_pickle("score_dataset.pickle")

train_df = pd.merge(text_df, score_df, on="email")
train_df.to_pickle("train_dataset.pickle")
