import numpy as np
import pandas as pd
from pathlib import Path
from ..constants import root


default_text_folder = root / "data" / "text_raw"
text_columns = pd.Index([ 'task1_text','task2_text','task3_text' ])


def parse_text(text_folder):
    text_folder = Path(text_folder)

    emails = []
    task_texts = [[], [], []]
    for user_folder in text_folder.iterdir():
        emails.append(user_folder.name)
        full_text = '' 
        for i in range(3):
            task_file = user_folder / f"task{i+1}.txt"
            if task_file.exists():
                with task_file.open() as f:
                    task_texts[i].append(f.read())
            else:
                task_texts[i].append(None)
            

    text_df = pd.DataFrame({
        'email': emails,
        'task1_text': task_texts[0],
        'task2_text': task_texts[1],
        'task3_text': task_texts[2],
        'text': [
            (texts[0] or "") + "\n" + (texts[1] or "") + "\n" + (texts[2] or "")
            if any(texts) else None
            for texts in zip(*task_texts)
        ]
    })

    return text_df


def get_text():
    return parse_text( default_text_folder )
