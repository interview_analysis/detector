from itertools import chain
import pandas as pd
from ..processing import tokenize
from ..embedding import muse
from .assessment import parse_assessment, default_assessment_file
from .text import parse_text, text_columns, default_text_folder


def __join_text_with_assessment(assessment_df, text_df, columns):
    subset = [col+postfix for col in columns for postfix in [" score", " words"]]
    result_df = pd.merge(text_df, assessment_df, how='left', on="email")
    result_df.dropna(axis='index', how='all', subset=subset, inplace=True)
    result_df = result_df[result_df.columns[:5].tolist() + subset]
    for col in columns:
        result_df[col+" tokens"] = result_df[col+" words"].apply(lambda s: [ word for word in str(s).strip().lower().replace(",", "").split(" ") ])
    result_df.reset_index(inplace=True, drop=True)
    return result_df


def parse_train(assessment_file, text_folder, columns=None, embeddings=None, phrase_extractor=None):
    assessment_df = parse_assessment(assessment_file)
    text_df = parse_text(text_folder)
    
    assessment_df.iloc[:, 5:] = assessment_df.iloc[:, 5:]

    if columns is None:
        columns = assessment_df.columns[5:]

    joined_df = __join_text_with_assessment(assessment_df, text_df, columns)
    joined_df.rename({column: column + ' words' for column in text_columns}, axis='columns', inplace=True)
    for column in text_columns:
        joined_df[column + ' tokens'] = joined_df[column + ' words'].apply(tokenize)

    if phrase_extractor is not None:
        def texts_to_phrases(row):
            result = list(chain(*[ phrase_extractor.transform(text) or [] for text in row ]))
            if len(result) == 0:
                return None
            return result
        joined_df['phrases'] = joined_df[text_columns + ' tokens'].apply(texts_to_phrases, axis=1)

    if embeddings is not None:
        for embedding in embeddings:
            for column in columns:
                joined_df[column + ' ' + embedding.__name__] = joined_df[column + " words"].apply(embedding)
            if phrase_extractor is not None:
                joined_df['phrases' + ' ' + embedding.__name__] = joined_df['phrases'].apply(
                    lambda phrases: embedding(phrases, batched=True) if phrases is not None else None)
    
    return joined_df


def get_train(columns=['мотивация'], embeddings=[muse], phrase_extractor=None):
    return parse_train(default_assessment_file, default_text_folder, columns=columns, embeddings=embeddings,        
        phrase_extractor=phrase_extractor)
