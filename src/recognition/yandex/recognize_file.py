import argparse
from pathlib import Path
from typing import Union
import boto3
import ffmpeg
import os
import requests
import time
import json


class FileRecognizer:
    def __init__(self, bucket_name:str, bucket_folder:Union[str, Path]="/", temp_folder:Union[str, Path]="/tmp/audio_recognition"):
        self.api_key = os.environ['API_KEY']
        self.bucket_name = bucket_name
        
        temp_folder = Path(temp_folder)
        temp_folder.mkdir(parents=True, exist_ok=True)
        working_filename = f"file_{os.getpid()}.wav"
        self.converted_file = temp_folder / (working_filename)
        self.bucket_file = Path(bucket_folder) / working_filename

        session = boto3.session.Session()
        self.s3 = session.client(
            service_name='s3',
            endpoint_url='https://storage.yandexcloud.net'
        )
        
    def __upload_file(self, file: Path):
        self.converted_file.unlink(missing_ok=True)
        (ffmpeg
            .input(file.as_posix())
            .output(self.converted_file.as_posix(), ac=1, ar=48000)
            .run(quiet=True)
        )
        self.s3.upload_file(self.converted_file.as_posix(), self.bucket_name, self.bucket_file.as_posix())
    
    def __delete_file(self):
        self.s3.delete_object(Bucket=self.bucket_name, Key=self.bucket_file.as_posix())
        self.converted_file.unlink(missing_ok=True)
    
    def __parse_result(self, req: dict):
        words = []
        for chunk in req['response']['chunks']:
            for word in chunk['alternatives'][0]['words']:
                word = word.copy()
                word['startTime'] = float(word['startTime'][:-1])
                word['endTime']   = float(word['endTime']  [:-1])
                del word['confidence']
                words.append(word)
        return words
    
    def __write_file(self, words, file: Path):
        file.parent.mkdir(parents=True, exist_ok=True)
        if file.suffix != ".json":
            file = Path(file.as_posix() + ".json")
        with open(file, "w") as f:
            json.dump(words, f, ensure_ascii=False)
        
    def __recognize_file(self):
        filelink = self.s3.generate_presigned_url('get_object', Params={'Bucket': self.bucket_name, 'Key': self.bucket_file.as_posix()})
        POST = "https://transcribe.api.cloud.yandex.net/speech/stt/v2/longRunningRecognize"
        body ={
            "config": {
                "specification": {
                    "languageCode": "ru-RU",
                    "model": "general",
                    "profanityFilter": "false",
                    "audioEncoding": "LINEAR16_PCM",
                    "sampleRateHertz": "48000",
                    "audioChannelCount": "1",
                }
            },
            "audio": {
                "uri": filelink
            }
        }
        header = {'Authorization': 'Api-Key {}'.format(self.api_key)}
        req = requests.post(POST, headers=header, json=body)

        data = req.json()
        id = data['id']
        while True:
            time.sleep(1)
            GET = "https://operation.api.cloud.yandex.net/operations/{id}"
            req = requests.get(GET.format(id=id), headers=header)
            req = req.json()
            if req['done']: break
        
        return self.__parse_result(req)
        
    
    def recognize(self, input_file: Union[str, Path], output_file=None) -> list:
        input_file = Path(input_file)
        self.__upload_file(input_file)
        words = self.__recognize_file()
        self.__delete_file()
        if output_file is not None:
            self.__write_file(words, Path(output_file))
        return words
    
    
def recognize_file(input_file: Union[str, Path], bucket_name:str, output_file=None, bucket_folder:Union[str, Path]="/", temp_folder:Union[str, Path]="/tmp/audio_recognition") -> list:
    recognizer = FileRecognizer(bucket_name, bucket_folder=bucket_folder, temp_folder=temp_folder)
    words = recognizer.recognize(input_file, output_file)

    
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("file")
    parser.add_argument("bucket_name")
    args = parser.parse_args()
    words = recognize_file(args.file, args.bucket_name)
    text = " ".join([word['word'] for word in words])
    print(text)
