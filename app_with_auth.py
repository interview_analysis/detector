import os
import json
from flask import Flask, request, Response
from flask_cors import CORS
from model import ModelCompose
from competence_detector import detect as detect_competences
from keyword_detector import detect as detect_keywords


app = Flask(__name__)
app.debug = True
app.config['SECRET_KEY'] = os.environ['FLASK_SECRET_KEY']
CORS(app)

# configuration
app.config.update({
    'COGNITO_REGION': 'us-east-1',
    'COGNITO_USERPOOL_ID': 'us-east-1_HuLG4GZul',

    # optional
    'COGNITO_APP_CLIENT_ID': '4pvsaalt1qdk1onhivu47k36uo',  # client ID you wish to verify user is authenticated against
    'COGNITO_CHECK_TOKEN_EXPIRATION': True,  # disable token expiration checking for testing purposes
    'COGNITO_JWT_HEADER_NAME': 'Authorization',
    'COGNITO_JWT_HEADER_PREFIX': 'Bearer',
})


# initialize extension
from flask_cognito import CognitoAuth, cognito_auth_required
cogauth = CognitoAuth(app)

@app.route('/detect_competences', methods=['POST'])
@cognito_auth_required
def detect_competences_endpoint():
    tokens = request.get_json()['tokens']
    response = detect_competences(tokens)
    return json.dumps( response )

@app.route('/detect_keywords', methods=['POST'])
@cognito_auth_required
def detect_keywords_endpoint():
    tokens = request.get_json()['tokens']
    token_indexes = detect_keywords(tokens)
    return json.dumps(token_indexes)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=3000)
