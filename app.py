import os
import json
from flask import Flask, request, Response
from flask_cors import CORS
from model import ModelCompose
from competence_detector import detect as detect_competences
from keyword_detector import detect as detect_keywords


app = Flask(__name__)
app.debug = True
app.config['SECRET_KEY'] = os.environ['FLASK_SECRET_KEY']
CORS(app)


@app.route('/detect_competences', methods=['POST'])
def detect_competences_endpoint():
    tokens = request.get_json()['tokens']
    response = detect_competences(tokens)
    return json.dumps( response )

@app.route('/detect_keywords', methods=['POST'])
def detect_keywords_endpoint():
    tokens = request.get_json()['tokens']
    token_indexes = detect_keywords(tokens)
    return {
        "indexes": token_indexes,
    }

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=3000)
