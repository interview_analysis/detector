import numpy as np

class ModelCompose():
    def __init__(self, model, competences_embeddings, balanced_weight_bias=None, reducer=None):
        self.model = model
        self.reducer = reducer
        X = []
        Y = []
        for label_index, competence_embeddings in enumerate(competences_embeddings):
            X.extend(competence_embeddings)
            Y.extend([label_index] * len(competence_embeddings))
        if reducer is not None:
            X = reducer.fit_transform(X)

        if balanced_weight_bias is not None:
            balanced_weight_bias = np.array(balanced_weight_bias)
            balanced_weight_bias = balanced_weight_bias / (sum(balanced_weight_bias) / len(balanced_weight_bias))
            weights = len(Y) / (len(competences_embeddings) * np.bincount(Y))
            weights *= balanced_weight_bias
            self.model.set_params(class_weight={ label: val for label, val in enumerate(weights) })

        self.model.fit(X, Y)

    def predict(self, embeddings):
        if self.reducer is not None:
            embeddings = self.reducer.transform(embeddings)
        predictions = self.model.predict(embeddings)
        return predictions

    def predict_proba(self, embeddings):
        if self.reducer is not None:
            embeddings = self.reducer.transform(embeddings)
        scores = self.model.predict_proba(embeddings)
        return scores

    def find_most_probable(self, embeddings, topk=1):
        phrases_scores = self.predict_proba(embeddings)
        indices = phrases_scores.argsort(axis=0)[-topk:]
        result_scores = np.array([ phrases_scores[indices[:, i], i] for i in range(phrases_scores.shape[1]) ]).T
        return indices, result_scores
