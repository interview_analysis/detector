FROM python:3.8-slim
WORKDIR /app

ENV FLASK_SECRET_KEY=jj523lkjn43n246

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY src ./src
COPY init.py ./
RUN python init.py

COPY navec_hudlit_v1_12B_500K_300d_100q.tar ./
COPY model.py ./
COPY model.pickle ./
COPY keywords.pickle ./

EXPOSE 3000

COPY competence_detector.py ./
COPY keyword_detector.py ./
COPY app.py ./
CMD ["python", "app.py"]
